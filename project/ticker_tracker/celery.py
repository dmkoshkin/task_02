from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ticker_tracker.settings')
 
app = Celery('ticker_tracker')
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()

app.conf.beat_schedule = {
    # Запускаем проверку новых данных по тикерам
    # фоновым заданием
    'checking-ticker-external-info': {
        'task': 'tracker_api.tasks.check_ticker_external_info',
        'schedule': 25.0
    },
}