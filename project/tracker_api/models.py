import datetime

from django.db import models

# Create your models here.

class UserTrackingInfo(models.Model):
    email = models.EmailField(blank=False)
    ticker = models.CharField(max_length=10, blank=False)
    max_price = models.CharField(max_length=20, null=True)
    min_price = models.CharField(max_length=20, null=True)
    last_refresh = models.DateTimeField(blank=True, default=datetime.datetime(2010, 1, 1))
    last_modified = models.DateTimeField(blank=True, default=datetime.datetime(2010, 1, 1))
 

    def save(self, *args, **kwargs):

        price_fields = ['max_price', 'min_price']
        if any([field in kwargs for field in price_fields]):
            for field in price_fields:
                setattr(self, field, kwargs.get(field, None))

        kwargs.clear()
        super().save(*args, **kwargs)     
