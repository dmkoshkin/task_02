import json, os

from jsonschema import validate
from jsonschema.exceptions import ValidationError

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.cache import cache
from django.conf import settings

from .json_validation import get_schema
from .models import UserTrackingInfo

# Create your views here.

# разрешим всем POSTить 
@csrf_exempt
def subscription_view(request):
    if request.method == "POST":
        body = request.body
        try:
            json_body = json.loads(body)
        except json.JSONDecodeError:
            return JsonResponse({'message': 'sended body is not a JSON'}, status=400)

        # Проверим входящий JSON
        try:
            json_schema = get_schema("subscription.json")
            if not json_schema:
                return JsonResponse({'message': 'error while getting a validation JSON-schema'}, status=500)
            validate(json_body, json_schema)
        except ValidationError as error:
            return JsonResponse({'message': f"parsing JSON error: {error.message}"}, status=400) 

        # Получим "чистый" объект
        cleaned_data = {
            'ticker': json_body.get('ticker'),
            'email': json_body.get('email')
        }

        other_fields = ['max_price', 'min_price']
        for field in other_fields:
            if field in json_body:
                cleaned_data[field] = str(json_body.get(field))

        # Проверяем количество разных подписок по этому email
        user_email = cleaned_data.get('email')
        entries_count = UserTrackingInfo.objects.filter(email=user_email).count()
        if entries_count == 5:
            return JsonResponse({'message': f'email {user_email} already have maximum subscriptions'}, status=503) 

        # либо создаем новую подписочку, либо обновляем существующую
        user_ticker = cleaned_data.get('ticker')
        try:
            obj = UserTrackingInfo.objects.get(email=user_email, ticker=user_ticker)
            obj.save(**cleaned_data)
            return JsonResponse({'message': 'user tracking info updated'}, status=200) 
        except UserTrackingInfo.DoesNotExist:
            obj = UserTrackingInfo.objects.create(**cleaned_data)
            return JsonResponse({'message': 'user tracking info created'}, status=201) 

    elif request.method == "DELETE":
        # забираем email из uri
        # если его нет - ругаемся
        user_email = request.GET.get('email', None)
        if not user_email:
            return JsonResponse({'message': 'email field must be provided'}, status=400) 
        
        # проверяем, отправили ли нам еще ticker
        user_ticker = request.GET.get('ticker', None)
        # в зависимости от начилия ticker, наш запрос меняется
        if user_ticker:
            query_set = UserTrackingInfo.objects.filter(email=user_email, ticker=user_ticker)
        else:
            query_set = UserTrackingInfo.objects.filter(email=user_email)

        query_set.delete()

        return JsonResponse({'message': f'user tracking info for {user_email} were deleted'}, status=204) 

