from django.test import TestCase
from django.shortcuts import reverse

from .models import UserTrackingInfo

import requests, json

# Create your tests here.

class TrackingInfoTestCase(TestCase):

    def test_database_is_empty(self):
        entries_count = UserTrackingInfo.objects.all().count()
        self.assertEqual(entries_count, 0)

    def test_add_wrong_data(self):
        data1 = {
            "ticker": "FB",
            # "email": "g.semenov@mail.ru",
            "min_price": 150,
            "max_price": 250
        }
        r = self.client.post(reverse('subscription'), data=data1, content_type='application/json')

        self.assertEqual(r.status_code, 400)
        self.assertEqual('email' in r.json()['message'], True)

        data2 = {
            #"ticker": "FB",
            "email": "g.semenov@mail.ru",
            "min_price": 150,
            "max_price": 25    
        }

        r = self.client.post(reverse('subscription'), data=data2, content_type='application/json')

        self.assertEqual(r.status_code, 400)
        self.assertEqual('ticker' in r.json()['message'], True)

    def test_add_tracking_data(self):

        data_iter = self.my_add_data_iteratior()

        my_data = {
            'act': None,
            'url': None,
            'data': None,
            'status_code': None,
            'content_type': None,
            'count': None
        }

        while True:
            try:
                data = next(data_iter)
                for key in my_data.keys():
                    if key in data:
                        my_data[key] = data.get(key)                
                
                if my_data['act'] == 'post':
                    action = self.client.post
                elif my_data['act'] == 'delete':
                    action = self.client.delete

                r = action(my_data['url'], data=my_data['data'], content_type=my_data['content_type'])

                self.assertEqual(r.status_code, my_data['status_code'])

                entries_count = UserTrackingInfo.objects.all().count()
                self.assertEqual(entries_count, my_data['count'])

            except StopIteration:
                break

    def my_add_data_iteratior(self):
        data = [
            {
                "act": "post",
                "url": reverse("subscription"),
                "data": {
                    "ticker": "FB",
                    "email": "semen.fedor@mail.ru",
                    "min_price": 150,
                    "max_price": 350
                },
                "status_code": 201,
                "content_type": 'application/json',
                "count": 1
            },
            {
                "data": {
                    "ticker": "TSLA",
                    "email": "semen.fedor@mail.ru",
                    "min_price": 250,
                    "max_price": 360
                },
                "count": 2
            },
            {  
                "data": {
                    "ticker": "CRM",
                    "email": "william@hotmail.com",
                    "min_price": 250,
                    "max_price": 610
                },
                "count": 3
            },
            {  
                "act": "delete",
                "url": reverse("subscription") + "?email=semen.fedor@mail.ru",
                "data": None,
                "status_code": 204,
                "count": 1
            },
            {  
                "url": reverse("subscription") + "?email=william@hotmail.com",
                "count": 0
            }
        ]

        for item in data:
            yield item