import json, os

from django.conf import settings

def get_schema(schema_file_name):
    schema_file_name = os.path.join(settings.TRACKER_API_SCHEMAS_PATH, schema_file_name)
    try:
        with open(schema_file_name) as schema_file:
            return json.loads(schema_file.read())
    except:
        return None