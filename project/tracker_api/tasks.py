from __future__ import absolute_import, unicode_literals

import time, requests, pytz

from django.utils import timezone
from django.core.cache import cache
from django.core.mail import send_mail as django_send_email

from dateutil import parser
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from ticker_tracker.celery import app

from .json_validation import get_schema
from .models import UserTrackingInfo
from .external_env import EXTERTANL_API_URL, EXTERTANL_API_DEFAULT_PARAMS

@app.task
def check_ticker_external_info():

    # Получим тикер, который который давно не обновлялся
    obj = UserTrackingInfo.objects.all().values("ticker").order_by("last_modified").first()
    if not obj:
        print('empty database')
        return
    
    user_ticker = obj.get('ticker')
    print(f"fetching {user_ticker}...")

    # Изменим структуру параметров uri для запроса со сторонненго сервиса
    EXTERTANL_API_DEFAULT_PARAMS['symbol'] = user_ticker
    response = requests.get(EXTERTANL_API_URL, params=EXTERTANL_API_DEFAULT_PARAMS)
    if response.status_code == 200:
        response_json = response.json()
        try:
            json_schema = get_schema("service_response.json")
            if not json_schema:
                print('error while getting a validation JSON-schema')
                return
            validate(response_json, json_schema)
        except ValidationError as error:
            print(f"parsing JSON error for ticker {user_ticker}: {error.message}")
            return
        # если в ответе есть секция "Error Message",
        # значит, что-то не удалось
        if response_json.get('Error Message', None) != None:
            print(f'error fetching {user_ticker} ticker info')
            print(response_json.get("Error Message"))
            return
        # получаем из ответа нужные нам поля
        last_refresh = response_json.get('Meta Data').get('3. Last Refreshed')
        last_data = response_json.get('Time Series (1min)').get(last_refresh)
        ticker_price = last_data.get('4. close')
        print(f'ticker price: {ticker_price} refresh: {last_refresh}')
    else:
        print(f'error while getting {user_ticker} ticker info')
        return

    # когда инфа о тикере получена, достанем всех пользователей, которые следят
    # за этим тикером
    user_set = UserTrackingInfo.objects.filter(ticker=user_ticker)

    for user_tracker in user_set:
        # проверим дату, хранящуюся в модели с датой обновления тикера
        if user_tracker.last_refresh < pytz.UTC.localize(parser.parse(last_refresh)):
            # если из внешнего сервиса пришло что-то свеженькое,
            # то заранее подготавливаем структуру для отправки письма
            email_send_data = {
                "email": user_tracker.email,
                "ticker": user_ticker,
                "new_price": ticker_price
            }
            # сравниваем стоимость тикера со значениями в модели
            if user_tracker.max_price is not None and float(user_tracker.max_price) < float(ticker_price):
                email_send_data['mode'] = 'max_price'
                email_send_data['price'] = user_tracker.max_price
                send_emails.delay(**email_send_data)
            elif user_tracker.min_price is not None and float(user_tracker.min_price) > float(ticker_price):
                email_send_data['mode'] = 'min_price'
                email_send_data['price'] = user_tracker.min_price
                send_emails.delay(**email_send_data)
            
            # обновляем в модели дату данных тикера
            user_tracker.last_refresh = parser.parse(last_refresh)
            
        user_tracker.last_modified = timezone.now()        
        user_tracker.save()           

@app.task
def send_emails(email, mode, ticker, price, new_price):
    body = f"Cost of {ticker} now is {'greater' if mode == 'max_price' else 'lower'} than {price} and equal {new_price}"
    print(f"send email to {email} with body {body}")

    # Отправка письма
    # django_send_email('Ticker cost has changed', body, 'noreplytickertracker@awesomemail.net', [email,])
