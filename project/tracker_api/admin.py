from django.contrib import admin
from .models import UserTrackingInfo

# Register your models here.

admin.site.register(UserTrackingInfo)
